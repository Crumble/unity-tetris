![Preview](https://i.imgur.com/ObbIoNB.mp4)

UNITY 2018.1

A super simple tetris game to get you started.

Has grids and several different types of blocks to spawn.

Get a complete line from left to right to clear the line and gain 10 points. See how many points you can get.

Use arrow keys to move the blocks Left and Right.

Press UP to rotate the piece
Press DOWN to speed up the falling.

You can test it [Here](https://crumble.gitlab.io/unity-tetris/)